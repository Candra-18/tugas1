function LuasTrapesium(x, y, z) {
  var Hasil;
   Hasil = (parseFloat(x) * (parseFloat(y) + parseFloat(z))) / 2;
  return Hasil;
}

function KelilingTrapesium(x, y, z, v) {
    var Hasil;
     Hasil = (parseFloat(x) + parseFloat(y) + parseFloat(z) + parseFloat(v) );
    return Hasil;
  }

  module.exports = {
    LuasTrapesium,
    KelilingTrapesium,
  };
