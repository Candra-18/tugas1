function LuasJajarGenjang(x, y) {
  var Hasil;
  Hasil = parseFloat(x) * parseFloat(y);
  return Hasil;
}

function KelilingJajarGenjang(x, y, z, v) {
  var Total;
  Total = (parseFloat(x) + parseFloat(y)) * (parseFloat(z) + parseFloat(v));
  return Total;
}

module.exports = {
  LuasJajarGenjang,
  KelilingJajarGenjang,
};
