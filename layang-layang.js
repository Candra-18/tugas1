//function menghitung keliling dan luas bangun datar Layang-layang
// x = diagonal 1
// y = diagonal 2
function LuasLayang(x,y){
    var hitung;
    hitung = 0.5 * parseFloat(x) * parseFloat(y);
    return hitung;
 }
 
 // x = panjang sisi x
 // y = panjang sisi y
 function KelilingLayang(x,y){
     var hasil;
     hasil = 2 * (parseFloat(x) + parseFloat(y));
     return hasil;
 }
 module.exports = {LuasLayang,KelilingLayang}