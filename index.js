// Readline
const readline2 = require("readline");
const rl = readline2.createInterface({
  input: process.stdin,
  output: process.stdout,
});
///

// Rumus Bangun Datar
var RumusBangunDatar = [
  require("./lingkaran.js"), //Mbak Maria
  require("./trapesium.js"), //Mas Chandra
  require("./segitiga.js"), //Mas Ariawan
  require("./layang-layang.js"), //Mbak Nilam
  require("./persegi-panjang.js"), //Mbak Maria
  require("./jajargenjang.js"), //Mas Chandra
  require("./belah-ketupat.js"), //Mas Ariawan
  require("./persegi.js"), //Mbak Nilam
];

// Rumus Bangun Datar
var RumusBangunRuang = [
  require("./kubus.js"), //Mbak Nilam
  require("./balok.js"), //Mbak Maria
  require("./prisma-segitiga.js"), //Mas Chandra
];

//Fungsi Manggil Rumus
function welcomes(a, b) {
  if (parseInt(a) == 1) {
    switch (parseInt(b)) {
      case 1:
        //Lingkaran
        console.log("+===Operasi Lingkaran===+");
        console.log("=========================");
        console.log("1.Luas Lingkaran\n2.Keliling Lingkaran");
        rl.question("Jawab :", (pilihmetode) => {
          if (parseInt(pilihmetode) == 1) {
            console.log("Masukan Jari - Jari : ");
            rl.question("Jawab :", (x) => {
              console.log(
                "Luas Lingkaran : " + RumusBangunDatar[0].LuasLingkaran(x)
              );
            });
          } else if (parseInt(pilihmetode) == 2) {
            console.log("Masukan Diameter :");
            rl.question("Jawab :", (x) => {
              console.log(
                "Keliling Lingkaran : " +
                  RumusBangunDatar[0].KelilingLingkaran(x)
              );
            });
          } else {
            console.log("Maaf Anda Salah Sasaran!");
          }
        });
        break;
      case 2:
        //Trapesium
        console.log("+===Operasi Trapesium===+");
        console.log("=========================");
        console.log("1.Luas Trapesium\n2.Keliling Trapesium");
        rl.question("Jawab :", (pilihmetode) => {
          if (parseInt(pilihmetode) == 1) {
            console.log("Masukan Tinggi : ");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Sisi Atas : ");
              rl.question("Jawab :", (y) => {
                console.log("Masukan Sisi Bawah : ");
                rl.question("Jawab :", (z) => {
                  console.log(
                    "Luas Trapesium : " +
                      RumusBangunDatar[1].LuasTrapesium(x, y, z)
                  );
                });
              });
            });
          } else if (parseInt(pilihmetode) == 2) {
            console.log("Masukan Atas : ");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Sisi Bawah : ");
              rl.question("Jawab :", (y) => {
                console.log("Masukan Sisi Kanan : ");
                rl.question("Jawab :", (z) => {
                  console.log("Masukan Sisi Kiri : ");
                  rl.question("Jawab :", (n) => {
                    console.log(
                      "Keliling Trapesium : " +
                        RumusBangunDatar[1].KelilingTrapesium(x, y, z, n)
                    );
                  });
                });
              });
            });
          } else {
            console.log("Maaf Anda Salah Sasaran!");
          }
        });
        break;
      case 3:
        //Segitiga
        console.log("+===Operasi Segitiga===+");
        console.log("=========================");
        console.log("1.Luas Segitiga\n2.Keliling Segitiga");
        rl.question("Jawab :", (pilihmetode) => {
          if (parseInt(pilihmetode) == 1) {
            console.log("Masukan Tinggi ");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Alas ");
              rl.question("Jawab :", (y) => {
                console.log(
                  "Luas Segitiga : " + RumusBangunDatar[2].LuasSegitiga(x, y)
                );
              });
            });
          } else if (parseInt(pilihmetode) == 2) {
            console.log("Masukan Sisi kanan :");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Sisi Kiri :");
              rl.question("Jawab :", (y) => {
                console.log("Masukan Alas :");
                rl.question("Jawab :", (z) => {
                  console.log(
                    "Keliling Segitiga : " +
                      RumusBangunDatar[2].KelilingSegitga(x, y, z)
                  );
                });
              });
            });
          } else {
            console.log("Maaf Anda Salah Sasaran!");
          }
        });
        break;
      case 4:
        // Layang-Layang
        console.log("+===Operasi Layang-Layang===+");
        console.log("=========================");
        console.log("1.Luas Layang-layang\n2.Keliling Layang-layang");
        rl.question("Jawab :", (pilihmetode) => {
          if (parseInt(pilihmetode) == 1) {
            console.log("Masukan Diagonal 1 : ");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Diagonal 2 : ");
              rl.question("Jawab :", (y) => {
                console.log(
                  "Luas Layang - layang : " +
                    RumusBangunDatar[3].LuasLayang(x, y)
                );
              });
            });
          } else if (parseInt(pilihmetode) == 2) {
            console.log("Masukan Panjang Sisi Atas :");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Panjang Sisi Bawah :");
              rl.question("Jawab :", (y) => {
                console.log(
                  "Keliling Lingkaran : " +
                    RumusBangunDatar[3].KelilingLayang(x, y)
                );
              });
            });
          } else {
            console.log("Maaf Anda Salah Sasaran!");
          }
        });
        break;
      case 5:
        //Pesegi Panjang
        console.log("+===Operasi Persegi Panjang===+");
        console.log("=========================");
        console.log("1.Luas Persegi Panjang\n2.Keliling Persegi Panjang");
        rl.question("Jawab :", (pilihmetode) => {
          if (parseInt(pilihmetode) == 1) {
            console.log("Masukan Panjang : ");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Lebar : ");
              rl.question("Jawab :", (y) => {
                console.log(
                  "Luas Persegi Panjang : " +
                    RumusBangunDatar[4].LuasPersegiPanjang(x, y)
                );
              });
            });
          } else if (parseInt(pilihmetode) == 2) {
            console.log("Masukan Panjang :");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Lebar :");
              rl.question("Jawab :", (y) => {
                console.log(
                  "Keliling Persegi Panjang : " +
                    RumusBangunDatar[4].KelilingPersegiPanjang(x, y)
                );
              });
            });
          } else {
            console.log("Maaf Anda Salah Sasaran!");
          }
        });
        break;
      case 6:
        //Jajargenjang
        console.log("+===Operasi Jajargenjang===+");
        console.log("=========================");
        console.log("1.Luas Jajargenjang\n2.Keliling Jajargenjang");
        rl.question("Jawab :", (pilihmetode) => {
          if (parseInt(pilihmetode) == 1) {
            console.log("Masukan Alas : ");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Tinggi ");
              rl.question("Jawab :", (y) => {
                console.log(
                  "Luas Jajargenjang : " +
                    RumusBangunDatar[5].LuasJajarGenjang(x, y)
                );
              });
            });
          } else if (parseInt(pilihmetode) == 2) {
            console.log("Masukan Sisi Kanan :");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Sisi Kiri :");
              rl.question("Jawab :", (y) => {
                console.log("Masukan Sisi Atas :");
                rl.question("Jawab :", (z) => {
                  console.log("Masukan Sisi Bawah :");
                  rl.question("Jawab :", (v) => {
                    console.log(
                      "Keliling Jajargenjang : " +
                        RumusBangunDatar[5].KelilingJajarGenjang(x, y, z, v)
                    );
                  });
                });
              });
            });
          } else {
            console.log("Maaf Anda Salah Sasaran!");
          }
        });
        break;
      case 7:
        //Belah Ketupat
        console.log("+===Operasi Belah Ketupat===+");
        console.log("=========================");
        console.log("1.Luas Belah Ketupat\n2.Keliling Belah Ketupat");
        rl.question("Jawab :", (pilihmetode) => {
          if (parseInt(pilihmetode) == 1) {
            console.log("Masukan Diagonal 1 ");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Diagonal 2 ");
              rl.question("Jawab :", (y) => {
                console.log(
                  "Luas Belah Ketupat : " +
                    RumusBangunDatar[6].LuasBelahKetupat(x, y)
                );
              });
            });
          } else if (parseInt(pilihmetode) == 2) {
            console.log("Masukan Sisi 1 :");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Sisi 2 :");
              rl.question("Jawab :", (y) => {
                console.log("Masukan Sisi 3 :");
                rl.question("Jawab :", (z) => {
                  console.log("Masukan Sisi 3 :");
                  rl.question("Jawab :", (n) => {
                    console.log(
                      "Keliling Belah Ketupat : " +
                        RumusBangunDatar[6].KelilingBelahKetupat(x, y, z, n)
                    );
                  });
                });
              });
            });
          } else {
            console.log("Maaf Anda Salah Sasaran!");
          }
        });
        break;
      case 8:
        //Persegi
        console.log("+===Operasi Persegi===+");
        console.log("=========================");
        console.log("Masukan Sisi : ");
        rl.question("Jawab :", (x) => {
          console.log("1.Luas Persegi\n2.Keliling Persegi");
          rl.question("Jawab :", (pilihmetode) => {
            if (parseInt(pilihmetode) == 1) {
              console.log(
                "Luas Persegi : " + RumusBangunDatar[7].LuasPersegi(x)
              );
            } else if (parseInt(pilihmetode) == 2) {
              console.log(
                "Keliling Persegi : " + RumusBangunDatar[7].KelilingPersegi(x)
              );
            } else {
              console.log("Maaf Anda Salah Sasaran!");
            }
          });
        });
        break;
    }
  } else if (parseInt(a) == 2) {
    switch (parseInt(b)) {
      case 1:
        //Kubus
        console.log("+===Operasi Kubus===+");
        console.log("=========================");
        console.log("Masukan Sisi : ");
        rl.question("Jawab :", (x) => {
          console.log("1.Luas Permukaan Kubus\n2.Volume Kubus");
          rl.question("Jawab :", (pilihmetode) => {
            if (parseInt(pilihmetode) == 1) {
              console.log(
                "Luas Permukaan Kubus : " + RumusBangunRuang[0].LuasKubus(x)
              );
            } else if (parseInt(pilihmetode) == 2) {
              console.log(
                "Volume Kubus : " + RumusBangunRuang[0].VolumeKubus(x)
              );
            } else {
              console.log("Maaf Anda Salah Sasaran!");
            }
          });
        });
        break;
      case 2:
        // Balok
        console.log("+===Operasi Balok===+");
        console.log("=========================");
        console.log("Masukan Panjang : ");
        rl.question("Jawab :", (x) => {
          console.log("Masukan Lebar : ");
          rl.question("Jawab :", (y) => {
            console.log("Masukan Tinggi : ");
            rl.question("Jawab :", (z) => {
              console.log("1.Luas Permukaan Balok\n2.Volume Balok");
              rl.question("Jawab :", (pilihmetode) => {
                if (parseInt(pilihmetode) == 1) {
                  console.log(
                    "Luas Permukaan Balok : " +
                      RumusBangunRuang[1].LuasPermukaanBalok(x, y, z)
                  );
                } else if (parseInt(pilihmetode) == 2) {
                  console.log(
                    "Volume Balok : " + RumusBangunRuang[1].VolumeBalok(x, y, z)
                  );
                } else {
                  console.log("Maaf Anda Salah Sasaran!");
                }
              });
            });
          });
        });
        break;
      case 3:
        //prisma-segitiga
        console.log("+===Operasi Prisma-Segitiga===+");
        console.log("=========================");
        console.log(
          "1.Luas Permikaan Prisma Segitiga\n2.Volume Prisma Segitiga"
        );
        rl.question("Jawab :", (pilihmetode) => {
          if (parseInt(pilihmetode) == 1) {
            console.log("Masukan Alas : ");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Tinggi Alas : ");
              rl.question("Jawab :", (y) => {
                console.log("Masukan Sisi 1 : ");
                rl.question("Jawab :", (z) => {
                  console.log("Masukan Sisi 2 : ");
                  rl.question("Jawab :", (n) => {
                    console.log("Masukan Sisi 3 : ");
                    rl.question("Jawab :", (m) => {
                      console.log("Masukan Tinggi: ");
                      rl.question("Jawab :", (v) => {
                        console.log(
                          "Luas Pemukaan Prisma Segitiga : " +
                            RumusBangunRuang[2].LuasPrismaSegitiga(
                              x,
                              y,
                              z,
                              n,
                              m,
                              v
                            )
                        );
                      });
                    });
                  });
                });
              });
            });
          } else if (parseInt(pilihmetode) == 2) {
            console.log("Masukan Alas :");
            rl.question("Jawab :", (x) => {
              console.log("Masukan Tinggi Alas:");
              rl.question("Jawab :", (y) => {
                console.log("Masukan Tinggi Prisma Segitiga :");
                rl.question("Jawab :", (z) => {
                  console.log(
                    "Volume Prisma Segitiga : " +
                      RumusBangunRuang[2].VolumePrismaSegitiga(x, y, z)
                  );
                });
              });
            });
          } else {
            console.log("Maaf Anda Salah Sasaran!");
          }
        });
        break;
    }
  }
}

// Tampilan Awal
console.log("\n");
console.log("\n");
console.log("Kalkulator Bangun Datar dan Bangun Ruang");
console.log("========================================");
console.log("______Chandra-Ariawan-Maria-Nilam_____");
console.log("\n");
console.log("Silakan Memilih :\n");
console.log("1. Bangun Datar\n2. Bangun Ruang\n");

// Petanyaan Awal
rl.question("Jawab :", (PilihBangun) => {
  if (PilihBangun == 1) {
    console.log("\n");
    console.log("+=====Bangun Datar=====+\n");
    console.log(
      "1.Lingkaran\n2.Trapesiun\n3.Segitiga\n4.Layang - Layang\n5.Persegi Panjang\n6.Jajar Genjang\n7.Belah Ketupat\n8.Persegi"
    );
    rl.question("Jawab :", (PilihBentuk) => {
      welcomes(PilihBangun, PilihBentuk);
    });
  } else if (PilihBangun == 2) {
    console.log("===Bangun Ruang===\n1. Kubus\n2. Balok\n3. Prisma Segitiga\n");
    rl.question("Jawab :", (PilihBentuk) => {
      //   bersihkan.clear();
      welcomes(PilihBangun, PilihBentuk);
    });
  } else {
    console.log("Jawaban Anda Ngawur!");
  }
});
