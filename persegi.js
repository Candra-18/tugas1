//function menghitung keliling dan luas bangun datar persegi
// x = sisi
function LuasPersegi(x){
   var hitung;
   hitung = parseFloat(x) * parseFloat(x);
   return hitung;
}

function KelilingPersegi(x){
    var hasil;
    hasil = 4 * parseFloat(x);
    return hasil;
}
module.exports = {LuasPersegi,KelilingPersegi}
