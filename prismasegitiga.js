function LuasPrismaSegitiga(x, y, v, z, n, m) {
  var Hasil;
  Hasil = 2 * ((parseFloat(x) * parseFloat(y)) / 2) + (parseFloat(z) + parseFloat(n) + parseFloat(m) * parseFloat(v));
  return Hasil;
}
function VolumePrismaSegitiga(x, y, z) {
  var Total;
  Total = ((parseFloat(x) * parseFloat(y)) / 2) * parseFloat(z);
  return Total;
}

module.exports = {
  LuasPrismaSegitiga,
  VolumePrismaSegitiga,
};
