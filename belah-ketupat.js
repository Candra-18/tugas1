function LuasBelahKetupat(x, y) {
  var Hasil;
  Hasil = (parseFloat(x) * parseFloat(y)) / 2;
  return Hasil;
}

function KelilingBelahKetupat(x, y, z, n) {
  var Hasil;
  Hasil = parseFloat(x) + parseFloat(y) + parseFloat(z) + parseFloat(n);
  return Hasil;
}

module.exports = {
  LuasBelahKetupat,
  KelilingBelahKetupat,
};
